package tkmarcacao.atualizate.nuno.gestaomarcacoes.Contollers;

import java.util.ArrayList;
import java.util.List;

import tkmarcacao.atualizate.nuno.gestaomarcacoes.domain.Cliente;
import tkmarcacao.atualizate.nuno.gestaomarcacoes.domain.Empresa;
import tkmarcacao.atualizate.nuno.gestaomarcacoes.domain.Marcacao;

/**
 * Created by nuno on 21/07/2015.
 */
public class AlterarMarcacaoController {

    private Empresa emp;

    public AlterarMarcacaoController(Empresa emp){
        this.emp = emp;
    }


    public boolean validaMarcacao(Marcacao x){
        for(Marcacao m: this.emp.getRegistoMarcacao().getListaMarcacoes()){
            if(x.getData().equals(m.getData())){
                return false;
            }
        }
        return true;
    }

    public boolean alterarMarc(Marcacao x, Cliente y){
        int index = 0;
        for(int i =0;i<this.getMarcacaoesCliente(y).size();i++){
            if(x.equals(this.getMarcacaoesCliente(y).get(i))){
                index = i;
                break;
            }
        }
        if(validaMarcacao(x)){
            this.emp.getRegistoMarcacao().getListaMarcacoes().set(index,x);
            return true;

        }
        else{
            return false;
        }
    }


    public List<Marcacao> getMarcacaoesCliente(Cliente x){
        List<Marcacao> mListaMarcacoesCliente = new ArrayList<>();
        for(Marcacao m: this.emp.getRegistoMarcacao().getListaMarcacoes()){
            if(m.getCliente().equals(x)) mListaMarcacoesCliente.add(m);
        }
        return mListaMarcacoesCliente;
    }

}
