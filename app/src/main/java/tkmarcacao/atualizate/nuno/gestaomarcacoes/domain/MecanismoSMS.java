package tkmarcacao.atualizate.nuno.gestaomarcacoes.domain;

import android.telephony.SmsManager;

/**
 * Created by nuno on 17-07-2015.
 */
public class MecanismoSMS implements MecanismoNotificacao {
    private SmsManager m_smsManager;

    @Override
    public boolean notifica(ProcessoNotificacao processo) {
        final Cliente cliente = processo.getCliente();
        final Marcacao marcacao = processo.getMarcacao();

        String sms = "Aviso: Tem uma marcação no dia " + marcacao.getData() + ". Obrigado.";

        try {
            m_smsManager = SmsManager.getDefault();
            m_smsManager.sendTextMessage(String.valueOf(cliente.getNunemroTelefone()), null, sms, null, null);
            marcacao.setInNotificada();

        } catch (Exception ex) {

        }


        return true;
    }

    @Override
    public String toString() {
        return "SMS";
    }
}
