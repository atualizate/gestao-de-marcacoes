package tkmarcacao.atualizate.nuno.gestaomarcacoes.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nuno on 16-07-2015.
 */
public class RegistoCliente {
    private List<Cliente> listaClientes;


    public RegistoCliente() {
        this.listaClientes = new ArrayList<>();
    }

    public List<Cliente> getListaClientes() {
        return listaClientes;
    }

    public Cliente novoCliente(){
        return new Cliente();
    }

    public boolean registarCliente(Cliente cliente){
        if(this.getListaClientes().contains(cliente)){
            return false;
        }
        return this.adicionarCliente(cliente);
    }

    private boolean adicionarCliente(Cliente cliente) {
        if(cliente.valida()){
            return this.getListaClientes().add(cliente);
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RegistoCliente that = (RegistoCliente) o;

        return getListaClientes().equals(that.getListaClientes());

    }

    @Override
    public int hashCode() {
        return getListaClientes().hashCode();
    }

    @Override
    public String toString() {
        return "RegistoCliente: " +
                " Lista de Clientes: " + listaClientes +
                '.';
    }
}
