package tkmarcacao.atualizate.nuno.gestaomarcacoes.domain;

import android.app.Activity;

import java.util.List;
import java.util.TimerTask;
//import android.telephony.SmsManager;

/**
 * Created by nuno on 16-07-2015.
 */
public class Notificacao  extends TimerTask implements ProcessoNotificacao{
    private Cliente m_cliente;
    private Marcacao m_marcacao;
    private Activity tela;


    public Notificacao() {
    }

    public Notificacao(Marcacao marcacao,Activity a) {
        this.m_cliente= marcacao.getCliente();
        this.m_marcacao= marcacao;
        this.tela=a;
    }

    @Override
    public void run() {
        this.notifica();
    }

    @Override
    public void notifica() {
     this.m_marcacao.getOpcao().getMecanismo().notifica(this);
    }

    public Cliente getCliente() {
        return m_cliente;
    }

    public Marcacao getMarcacao() {
        return m_marcacao;
    }

    public Activity getTela(){
        return this.tela;
    }
}
