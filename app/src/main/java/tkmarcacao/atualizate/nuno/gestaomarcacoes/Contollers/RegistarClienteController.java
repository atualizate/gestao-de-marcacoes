package tkmarcacao.atualizate.nuno.gestaomarcacoes.Contollers;

import tkmarcacao.atualizate.nuno.gestaomarcacoes.domain.Cliente;
import tkmarcacao.atualizate.nuno.gestaomarcacoes.domain.Empresa;
import tkmarcacao.atualizate.nuno.gestaomarcacoes.domain.RegistoCliente;

/**
 * Created by nuno on 16-07-2015.
 */
public class RegistarClienteController {
    private Empresa empresa;
    private RegistoCliente registoClientes;
    private Cliente cliente;


    public RegistarClienteController(Empresa empresa) {
        this.empresa = empresa;
        this.registoClientes = empresa.getRegistoClientes();
    }

    public void novoCliente(){
        this.cliente = this.registoClientes.novoCliente();
    }

    public void setDados(String nome, String email, String observacoes, int nunemroTelefone){
        this.cliente.setNome(nome);
        this.cliente.setEmail(email);
        this.cliente.setObservacoes(observacoes);
        this.cliente.setNunemroTelefone(nunemroTelefone);
    }

    public boolean registaCliente(){
        return this.registoClientes.registarCliente(this.cliente);
    }

}
