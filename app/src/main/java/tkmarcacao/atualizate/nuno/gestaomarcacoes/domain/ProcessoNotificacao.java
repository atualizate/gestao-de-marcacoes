package tkmarcacao.atualizate.nuno.gestaomarcacoes.domain;


import android.app.Activity;

import java.util.List;

/**
 * Created by nuno on 17-07-2015.
 */
public interface ProcessoNotificacao {
    void notifica();
    Cliente getCliente();
    Marcacao getMarcacao();
    Activity getTela();

}
