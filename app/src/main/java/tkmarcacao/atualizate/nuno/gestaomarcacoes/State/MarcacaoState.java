package tkmarcacao.atualizate.nuno.gestaomarcacoes.State;

/**
 * Created by nuno on 16-07-2015.
 */
public interface MarcacaoState {
    boolean valida();

    boolean isInCriada();
    boolean isInRegistada();
    boolean isInNotificada();
    boolean isInCancelada();

    boolean setInRegistada();
    boolean setInNotificada();
    boolean setInCancelada();

}
