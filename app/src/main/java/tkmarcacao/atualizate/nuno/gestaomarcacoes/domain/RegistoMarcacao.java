package tkmarcacao.atualizate.nuno.gestaomarcacoes.domain;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by nuno on 17-07-2015.
 */
public class RegistoMarcacao {

    private List<Marcacao> listaMarcacoes;

    public RegistoMarcacao() {
        this.listaMarcacoes=new ArrayList<>();
    }

    public List<Marcacao> getListaMarcacoes() {
        return listaMarcacoes;
    }

    public void setListaMarcacoes(List<Marcacao> listaMarcacoes) {
        this.listaMarcacoes = listaMarcacoes;
    }

    public Marcacao novaMarcacao(){
        return new Marcacao();
    }

    public boolean registaMarcacao(Marcacao marcacao){
        if(this.getListaMarcacoes().contains(marcacao)){
            return false;
        }
        return adicionarMarcacao(marcacao);
    }

    private boolean adicionarMarcacao(Marcacao marcacao) {
        if(marcacao.valida()){
            marcacao.setInRegistada();
            return this.getListaMarcacoes().add(marcacao);
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RegistoMarcacao)) return false;

        RegistoMarcacao that = (RegistoMarcacao) o;

        return listaMarcacoes.equals(that.listaMarcacoes);

    }

    @Override
    public int hashCode() {
        return listaMarcacoes.hashCode();
    }

    @Override
    public String toString() {
        return "Registo Marcações: " +
                " lista Marcações=" + listaMarcacoes +
                '.';
    }
}
