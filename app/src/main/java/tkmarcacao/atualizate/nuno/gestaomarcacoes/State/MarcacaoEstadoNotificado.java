package tkmarcacao.atualizate.nuno.gestaomarcacoes.State;

import tkmarcacao.atualizate.nuno.gestaomarcacoes.domain.Marcacao;

/**
 * Created by nuno on 16-07-2015.
 */
public class MarcacaoEstadoNotificado extends MarcacaoImpl{

    public MarcacaoEstadoNotificado(Marcacao marcacao) {
        super(marcacao);
    }

    public boolean valida(){
        return true;
    }

    @Override
    public boolean isInNotificada(){
        return false;
    }

    @Override
    public String toString() {
        return "Marcação Notificada.";
    }
}
