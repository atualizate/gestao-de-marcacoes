package tkmarcacao.atualizate.nuno.gestaomarcacoes.domain;

import java.util.Objects;

/**
 * Created by nuno on 16-07-2015.
 */
public class Cliente implements Cloneable, Comparable<Cliente>{
    private String nome;
    private String email;
    private String observacoes;
    private Integer nunemroTelefone;

    public Cliente(){
    }

    public Cliente(String nome, String email, String observacoes, int nunemroTelefone){
        this.setNome(nome);
        this.setEmail(email);
        this.setObservacoes(observacoes);
        this.setNunemroTelefone(nunemroTelefone);
    }

    public Cliente(Cliente outroCliente){
        this(outroCliente.getNome(), outroCliente.getEmail(), outroCliente.getObservacoes(), outroCliente.getNunemroTelefone());
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        if(nome.isEmpty()){
            throw new IllegalArgumentException("Nome inválido");
        }
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public int getNunemroTelefone() {
        return nunemroTelefone;
    }

    public void setNunemroTelefone(int nunemroTelefone) {
        this.nunemroTelefone = nunemroTelefone;
    }

    public boolean valida(){
        if(this.getNome()==null || this.getNome().isEmpty())
         return false;

        if(this.getNunemroTelefone()==0)
            return false;

         return true;
    }


    @Override
    public String toString() {
        return "Cliente:" +
                " Nome: '" + nome + '\'' +
                ", Email: '" + email + '\'' +
                ", Observacoes: '" + observacoes + '\'' +
                ", Nunemro Telefone: " + nunemroTelefone +
                '.';
    }

    @Override
    public boolean equals(Object o) {
        if(o == null){
            return false;
        }

        if(getClass() != o.getClass()){
            return false;
        }

        final Cliente outro=(Cliente) o;

        if(!this.getNome().equalsIgnoreCase(outro.getNome()) && this.getNunemroTelefone() != outro.getNunemroTelefone() && !this.getEmail().equals(outro.getEmail()) ){
            return false;
        }
        return true;

    }

    @Override
    public int hashCode() {
        int result = getNome().hashCode();
        result = 31 * result + getNunemroTelefone();
        return result;
    }

    @Override
    public Cliente clone(){
        return new Cliente(this);
    }


    @Override
    public int compareTo(Cliente another) {
        return this.getNome().compareTo(another.getNome());
    }
}
