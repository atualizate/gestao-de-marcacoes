package tkmarcacao.atualizate.nuno.gestaomarcacoes.domain;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by nuno on 17-07-2015.
 */
public class ListaOpcoes {
    private List<Opcao> listaOpcoes;

    public ListaOpcoes() {
        this.listaOpcoes=new ArrayList<>();
    }

    public List<Opcao> getListaOpcoes() {
        return listaOpcoes;
    }

    public void setListaOpcoes(List<Opcao> listaOpcoes) {
        this.listaOpcoes = listaOpcoes;
    }

    public Opcao novaOpcao(){
        return new Opcao();
    }

    public boolean registaOpcao(Opcao opcao){
        if(this.getListaOpcoes().contains(opcao)){
            return false;
        }
        return adicionaCliente(opcao);
    }

    private boolean adicionaCliente(Opcao opcao) {
        if(opcao.valida()){
            return this.getListaOpcoes().add(opcao);
        }
       return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ListaOpcoes that = (ListaOpcoes) o;

        return !(getListaOpcoes() != null ? !getListaOpcoes().equals(that.getListaOpcoes()) : that.getListaOpcoes() != null);

    }

    @Override
    public int hashCode() {
        return getListaOpcoes() != null ? getListaOpcoes().hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Lista Opcoes: " +
                " lista opções=" + listaOpcoes +
                '.';
    }


}
