package tkmarcacao.atualizate.nuno.gestaomarcacoes.UI;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import tkmarcacao.atualizate.nuno.gestaomarcacoes.Contollers.OpcaoController;
import tkmarcacao.atualizate.nuno.gestaomarcacoes.R;
import tkmarcacao.atualizate.nuno.gestaomarcacoes.domain.Empresa;
import tkmarcacao.atualizate.nuno.gestaomarcacoes.domain.MecanismoNotificacao;

/**
 * Created by nuno on 17-07-2015.
 */

public class RegistarOpcaoUI extends DialogFragment {
    private OpcaoController controller;
    private Empresa empresa;
    private View novaOpcao;
    private MecanismoNotificacao m;
    private EditText texto;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        this.controller = new OpcaoController(empresa);
        this.novaOpcao = inflater.inflate(R.layout.registar_opcoes, null);


        Spinner spinner = (Spinner) novaOpcao.findViewById(R.id.tiposNotificacao);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.notificaceos, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                m = empresa.getMecanismos().get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                m = empresa.getMecanismos().get(0);
            }
        });

        texto = (EditText) getActivity().findViewById(R.id.editText);


        builder.setView(novaOpcao)
                // Add action buttons
                .setPositiveButton(R.string.nova_opcao, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        try {

                            EditText nome = (EditText) novaOpcao.findViewById(R.id.nomeOpcao);
                            EditText difHora = (EditText) novaOpcao.findViewById(R.id.difHoras);
                            controller.novaOpcao();
                            controller.setNome(nome.getText().toString());
                            controller.setMecanismo(m);
                            controller.setDifHoras(Integer.parseInt(difHora.getText().toString()));
                            controller.registaOpcao();
                            texto.setText(empresa.getListaOpcoes().getListaOpcoes().toString());

                        } catch (Exception ex) {
                            texto.setText("opcao nula");
                        }


                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        RegistarOpcaoUI.this.getDialog().cancel();
                    }
                });
        return builder.create();
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

}
