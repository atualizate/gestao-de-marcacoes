package tkmarcacao.atualizate.nuno.gestaomarcacoes.State;

import tkmarcacao.atualizate.nuno.gestaomarcacoes.domain.Marcacao;

/**
 * Created by nuno on 16-07-2015.
 */
public class MarcacaoEstadoRegistado extends MarcacaoImpl {

    public MarcacaoEstadoRegistado(Marcacao marcacao) {
        super(marcacao);
    }

    @Override
    public boolean valida(){
        return true;
    }

    @Override
    public boolean isInRegistada(){
        return true;
    }

    @Override
    public boolean setInNotificada(){
        if (valida())
            return getMarcacao().setEstado(new MarcacaoEstadoNotificado(getMarcacao()));
        return false;
    }

    @Override
    public boolean setInCancelada(){
        if (valida())
            return getMarcacao().setEstado(new MarcacaoEstadoCancelada(getMarcacao()));
        return false;
    }

    @Override
    public String toString() {
        return "Marcação Registada";
    }
}
