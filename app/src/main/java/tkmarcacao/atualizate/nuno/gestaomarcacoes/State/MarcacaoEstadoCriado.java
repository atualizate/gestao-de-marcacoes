package tkmarcacao.atualizate.nuno.gestaomarcacoes.State;

import tkmarcacao.atualizate.nuno.gestaomarcacoes.domain.Marcacao;

/**
 * Created by nuno on 16-07-2015.
 */
public class MarcacaoEstadoCriado extends MarcacaoImpl {

    public MarcacaoEstadoCriado(Marcacao marcacao) {
        super(marcacao);
        marcacao.setEstado(this);
    }

    @Override
    public boolean valida(){
        return true;
    }

    @Override
    public boolean isInCriada(){
        return true;
    }

    @Override
    public boolean setInRegistada(){
        if (valida())
            return getMarcacao().setEstado(new MarcacaoEstadoRegistado(getMarcacao()));
        return false;
    }

    @Override
    public String toString() {
        return "Marcaçao Criada";
    }
}
