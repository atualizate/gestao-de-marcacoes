package tkmarcacao.atualizate.nuno.gestaomarcacoes.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Asus on 17/07/2015.
 */
public class RegistoMarcacoes {

    private List<Marcacao> listaMarcacoes;

    public RegistoMarcacoes() {
        listaMarcacoes = new ArrayList<Marcacao>();
    }

    public List<Marcacao> getListaMarcacoes() {
        return listaMarcacoes;
    }

    public void setListaMarcacoes(List<Marcacao> listaMarcacoes) {
        this.listaMarcacoes = listaMarcacoes;
    }

    public boolean adicionarMarcacao(Marcacao marcacao) {
        if(marcacao.valida()) {
            return listaMarcacoes.add(marcacao);
        } else {
            return false;
        }
    }

    public boolean removerMarcacao(Marcacao marcacao) {
        if(listaMarcacoes.contains(marcacao)) {
            return listaMarcacoes.remove(marcacao);
        } else {
            return listaMarcacoes.remove(marcacao);
        }
    }
}

