package tkmarcacao.atualizate.nuno.gestaomarcacoes.domain;

/**
 * Created by nuno on 17-07-2015.
 */
public interface MecanismoNotificacao {
    boolean notifica(ProcessoNotificacao processo);

}
