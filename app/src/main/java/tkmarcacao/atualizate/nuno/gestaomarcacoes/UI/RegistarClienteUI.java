package tkmarcacao.atualizate.nuno.gestaomarcacoes.UI;

import android.app.DialogFragment;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;


import tkmarcacao.atualizate.nuno.gestaomarcacoes.Contollers.RegistarClienteController;
import tkmarcacao.atualizate.nuno.gestaomarcacoes.R;
import tkmarcacao.atualizate.nuno.gestaomarcacoes.domain.Empresa;


/**
 * Created by nuno on 17-07-2015.
 */

public class RegistarClienteUI extends DialogFragment {

    private RegistarClienteController controller;
    private Empresa empresa;
    private View registarCliente;

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        controller = new RegistarClienteController(empresa);

        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        registarCliente = inflater.inflate(R.layout.registar_cliente, null);

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(registarCliente)
                // Add action buttons
                .setPositiveButton(R.string.novo_registo, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {


                        controller = new RegistarClienteController(empresa);

                        EditText m = (EditText) getActivity().findViewById(R.id.editText);

                        try {
                            EditText nome = (EditText) registarCliente.findViewById(R.id.nome);
                            EditText email = (EditText) registarCliente.findViewById(R.id.email);
                            EditText nota = (EditText) registarCliente.findViewById(R.id.observacoes);
                            EditText numero = (EditText) registarCliente.findViewById(R.id.numero);

                            controller.novoCliente();
                            controller.setDados(nome.getText().toString(), email.getText().toString(),
                                    nota.getText().toString(), Integer.parseInt(numero.getText().toString()));
                            controller.registaCliente();

                            m.setText(empresa.getRegistoClientes().getListaClientes().toString());

                        } catch (Exception e) {
                            m.setText("nao consegue");
                        }


                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        RegistarClienteUI.this.getDialog().cancel();
                    }
                });
        return builder.create();

    }

    public void setEmpresa(Empresa empresa){
        this.empresa=empresa;
    }

}