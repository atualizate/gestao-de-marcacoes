package tkmarcacao.atualizate.nuno.gestaomarcacoes.domain;


import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Date;
import java.util.List;

/**
 * Created by nuno on 16-07-2015.
 */
public class Empresa {
    private Timer timer;
    private RegistoCliente registoClientes;
    private RegistoMarcacao registoMarcacao;
    private ListaOpcoes listaOpcoes;
    private List<MecanismoNotificacao> mecanismos;


    public Empresa(){
        this.timer=new Timer();
        this.registoClientes=new RegistoCliente();
        this.listaOpcoes=new ListaOpcoes();
        this.registoMarcacao=new RegistoMarcacao();
        this.mecanismos=new ArrayList<>();
    }

    public void schedule(TimerTask task, Date date) {
        this.timer.schedule(task, date);
    }


    public RegistoCliente getRegistoClientes() {
        return registoClientes;
    }

    public ListaOpcoes getListaOpcoes() {
        return listaOpcoes;
    }

    public RegistoMarcacao getRegistoMarcacao() {
        return registoMarcacao;
    }

    public List<MecanismoNotificacao> getMecanismos() {
        this.mecanismos.add(new MecanismoEmail());
        this.mecanismos.add(new MecanismoSMS());
        return mecanismos;
    }
}
