package tkmarcacao.atualizate.nuno.gestaomarcacoes.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Asus on 17/07/2015.
 */
public class RegistoClientes {

    private List<Cliente> listaClientes;

    public RegistoClientes() {
        listaClientes = new ArrayList<>();
    }

    public void setListaClientes(List<Cliente> lst) {
        this.listaClientes = lst;
    }

    public List<Cliente> getListaClientes() {
        return this.listaClientes;
    }

    public boolean adicionarCliente(Cliente cliente) {
        if(cliente.valida()) {
            return this.listaClientes.add(cliente);
        } else {
            return false;
        }
    }

    public boolean removerCliente(Cliente cliente) {
        if(this.listaClientes.contains(cliente)) {
            return this.listaClientes.remove(cliente);
        } else {
            return false;
        }

    }

}
