package tkmarcacao.atualizate.nuno.gestaomarcacoes.domain;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.widget.EditText;

import tkmarcacao.atualizate.nuno.gestaomarcacoes.R;

/**
 * Created by nuno on 17-07-2015.
 */
public class MecanismoEmail implements MecanismoNotificacao {

    @Override
    public boolean notifica(ProcessoNotificacao processo) {

        final Cliente cliente = processo.getCliente();
        final Marcacao marcacao = processo.getMarcacao();
        final Activity tela=processo.getTela();

        String sms = "Aviso: Tem uma marcação no dia " + marcacao.getData() + ". Obrigado.";

        EditText m = (EditText) tela.findViewById(R.id.editText);

        try {
            Intent send = new Intent(Intent.ACTION_SENDTO);
            String uriText = "mailto:" + Uri.encode(cliente.getEmail()) +
                    "?subject=" + Uri.encode("Aviso Marcação") +
                    "&body=" + Uri.encode(sms);
            Uri uri = Uri.parse(uriText);

            send.setData(uri);

            tela.startActivity(Intent.createChooser(send, "Send mail..."));
            marcacao.setInNotificada();
            m.setText("enviado");

        } catch (Exception ex) {
            m.setText("nao enviou");
        }
        return true;
    }

    @Override
    public String toString() {
        return "Email";
    }
}
