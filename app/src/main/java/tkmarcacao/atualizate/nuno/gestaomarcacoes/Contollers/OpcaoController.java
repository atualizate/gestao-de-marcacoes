package tkmarcacao.atualizate.nuno.gestaomarcacoes.Contollers;

import tkmarcacao.atualizate.nuno.gestaomarcacoes.domain.Empresa;
import tkmarcacao.atualizate.nuno.gestaomarcacoes.domain.ListaOpcoes;
import tkmarcacao.atualizate.nuno.gestaomarcacoes.domain.MecanismoNotificacao;
import tkmarcacao.atualizate.nuno.gestaomarcacoes.domain.Opcao;

import java.util.List;

/**
 * Created by nuno on 17-07-2015.
 */
public class OpcaoController {
    private Empresa empresa;
    private ListaOpcoes listaOpcoes;
    private Opcao opcao;

    public OpcaoController(Empresa empresa) {
        this.empresa = empresa;
        this.listaOpcoes=this.empresa.getListaOpcoes();
    }

    public List<Opcao> getLista(){
        return this.listaOpcoes.getListaOpcoes();
    }

    public void novaOpcao(){
        this.opcao=this.listaOpcoes.novaOpcao();
    }

    public void setDifHoras(Integer diferencaHoras){
       this.opcao.setDiferencaHoras(diferencaHoras);
    }

    public void setNome(String nome){
        this.opcao.setNome(nome);
    }

    public void setMecanismo(MecanismoNotificacao mecanismo){
        this.opcao.setMecanismo(mecanismo);
    }

    public boolean registaOpcao(){
        return this.listaOpcoes.registaOpcao(this.opcao);
    }
}
