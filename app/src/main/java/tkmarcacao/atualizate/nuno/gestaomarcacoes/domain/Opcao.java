package tkmarcacao.atualizate.nuno.gestaomarcacoes.domain;

/**
 * Created by nuno on 17-07-2015.
 */
public class Opcao implements Comparable<Opcao>{
    private String nome;
    private MecanismoNotificacao mecanismo;
    private Integer diferencaHoras;

    public Opcao(){
    }

    public Opcao(String nome, MecanismoNotificacao mecanismo, Integer diferencaHoras) {
        this.nome = nome;
        this.mecanismo = mecanismo;
        this.diferencaHoras = diferencaHoras;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        if(nome.isEmpty()){
            throw new IllegalArgumentException("Nome em falta");
        }
        this.nome = nome;
    }

    public MecanismoNotificacao getMecanismo() {
        return mecanismo;
    }

    public void setMecanismo(MecanismoNotificacao mecanismo) {
        this.mecanismo = mecanismo;
    }

    public Integer getDiferencaHoras() {
        return diferencaHoras;
    }

    public void setDiferencaHoras(Integer diferencaHoras) {
        this.diferencaHoras = diferencaHoras;
    }

    public boolean valida(){
        if(this.getMecanismo()==null)
            return false;

        if(this.getNome()==null || this.getNome().isEmpty())
            return false;

        if(this.getDiferencaHoras()<=0)
            return false;

        return true;
    }

    @Override
    public String toString() {
        return "Opçao:" +
                " Nome: '" + nome + '\'' +
                ", Mecanismo: " + mecanismo +
                ", Diferenca de Horas: " + diferencaHoras +
                '.';
    }

    @Override
    public int compareTo(Opcao another) {
        return this.getNome().compareTo(another.getNome());
    }
}
