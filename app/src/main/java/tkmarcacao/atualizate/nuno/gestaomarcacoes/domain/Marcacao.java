package tkmarcacao.atualizate.nuno.gestaomarcacoes.domain;

import java.util.Calendar;

import tkmarcacao.atualizate.nuno.gestaomarcacoes.State.MarcacaoEstadoCriado;
import tkmarcacao.atualizate.nuno.gestaomarcacoes.State.MarcacaoState;

/**
 * Created by nuno on 16-07-2015.
 */
public class Marcacao implements Comparable<Marcacao> {
    private Cliente cliente;
    private Calendar data;
    private Opcao opcao;
    private MarcacaoState estado;

    public Marcacao(){
        new MarcacaoEstadoCriado(this);
    }

    public Marcacao(Cliente cliente, Calendar data, Opcao opcao) {
        this();
        this.setCliente(cliente);
        this.setData(data);
        this.setMecanismo(opcao);
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Calendar getData() {
        return data;
    }

    public void setData(Calendar data) {
        this.data = data;
    }

    public Opcao getOpcao() {
        return this.opcao;
    }

    public void setMecanismo(Opcao opcao) {
        this.opcao = opcao;
    }

    public MarcacaoState getEstado() {
        return estado;
    }

    public boolean setEstado(MarcacaoState estado){
        this.estado=estado;
        return true;
    }

    public boolean isInCriada(){
        return this.estado.isInCriada();
    }

    public boolean isInRegistada(){
        return this.estado.isInRegistada();
    }

    public boolean isInNotificada(){
        return this.estado.isInNotificada();
    }

    public boolean isInCancelada(){
        return this.estado.isInCancelada();
    }

    public boolean setInRegistada(){
        return this.estado.setInRegistada();
    }

    public boolean setInNotificada(){
        return this.estado.setInNotificada();
    }

    public boolean setInCancelada(){
        return this.estado.setInCancelada();
    }

    public boolean valida(){
        return this.estado.valida();
    }

    @Override
    public String toString() {
        return "Marcacao: " +
                " Cliente: " + cliente +
                ", data: " + data +
                ", Opção: " + opcao +
                ", estado: " + estado +
                '.';
    }

   @Override
    public int compareTo(Marcacao another) {
       return this.getData().compareTo(another.getData());
    }

    @Override
    public boolean equals(Object o){
        if(o==null){
            return false;
        }

        if(getClass() != o.getClass()){
            return false;
        }

        final Marcacao m=(Marcacao) o;

        if(this.getData().compareTo(m.getData())!=0){
            return false;
        }

        return true;
    }

}
