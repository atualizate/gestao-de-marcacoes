package tkmarcacao.atualizate.nuno.gestaomarcacoes.State;

import tkmarcacao.atualizate.nuno.gestaomarcacoes.domain.Marcacao;

/**
 * Created by nuno on 16-07-2015.
 */
public abstract class MarcacaoImpl implements MarcacaoState {
    private Marcacao marcacao;

    public MarcacaoImpl(Marcacao marcacao) {
        this.setMarcacao(marcacao);
    }

    public Marcacao getMarcacao() {
        return marcacao;
    }

    public void setMarcacao(Marcacao marcacao) {
        this.marcacao = marcacao;
    }

    @Override
    public boolean isInCriada(){
        return false;
    }

    @Override
    public boolean isInRegistada(){
        return false;
    }

    @Override
    public boolean isInNotificada(){
        return false;
    }

    @Override
    public boolean isInCancelada(){
        return false;
    }

    @Override
    public boolean setInRegistada(){
        return false;
    }

    @Override
    public boolean setInNotificada(){
        return false;
    }

    @Override
    public boolean setInCancelada(){
        return false;
    }
}
