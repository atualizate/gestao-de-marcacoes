package tkmarcacao.atualizate.nuno.gestaomarcacoes.State;

import tkmarcacao.atualizate.nuno.gestaomarcacoes.domain.Marcacao;

/**
 * Created by nuno on 16-07-2015.
 */
public class MarcacaoEstadoCancelada extends MarcacaoImpl{
    public MarcacaoEstadoCancelada(Marcacao marcacao) {
        super(marcacao);
    }

    @Override
    public boolean valida(){
        return true;
    }

    @Override
    public boolean isInCancelada(){
        return true;
    }

    @Override
    public String toString() {
        return "Marcaçao Cancelada";
    }
}
